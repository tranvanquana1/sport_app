<?php


namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\User\UserRepository;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index() {
        $users = $this->userRepository->getAll();

        return response()->json([
            'success' => true,
            'data' => $users
        ]);
    }

    public function store(Request $request) {
        $data = $request->all();

        $this->userRepository->create($data);

        return response()->json([
            'success' => true,
            'message' => 'Them moi thanh cong'
        ]);
    }
}
